import math

x1, x2 = [float(e) for e in input().split()]
y1, y2 = [float(e) for e in input().split()]

d = math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))

print(d)
